import yaml, copy
from datetime import datetime

POINTNAMES = ['fam', 'hlt', 'soc', 'ment', 'fin', 'org', 'val1', 'val2']

def now():
    return datetime.now()

def get_stamp(now):
    return now.strftime('%Y-%m-%d-%H%M')

def get_today(now):
    return ['%A, %m-%d']

class Save_State:

    def __init__ (self, player=None, schedule=None):
        self.player = player
        self.schedule = schedule

    def load(self, fn):
        with open(fn) as fh:
            try:
                yam = yaml.safe_load(fh))
            except yaml.YAMLError as exc:
                print(exc)
        self.player = Player(yam['player'])
        self.schedule = Schedule(yam['schedule'])

    def save(self):
        pass

class Player:

    def __init__(self, stats_yam=None):
        if stats_yam is not None:
            self.stats = copy(stats_yam)
        else:
            pass
            #initialize stats
    
    def verify(self):
        pass

class Schedule:

    def __init__(self, stats_yam=None):
        if stats_yam is not None:
            self.stats = copy(stats_yam)
        else:
            pass
            #initialize stats

    def verify(self):
        pass